# coding: utf-8
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable
from src.layers import EncoderLayer, DecoderLayer
from src.embed import Embedder, PositionalEncoder
from src.sublayers import FeedForward, MultiHeadAttention, Norm
import os


class Encoder(nn.Module):
    def __init__(self, attn, embed, pe, norm, ff, dropout_layer,
                 vocab_size, d_vocab, d_hidden, heads,
                 max_seq_len):
        super().__init__()
        self.embed = embed
        self.pe = pe
        self.layer = EncoderLayer(attn, norm, ff, dropout_layer,
                                  d_hidden, heads)
        self.dropout = dropout_layer
        self.norm = norm

    def forward(self, src, mask):
        x = self.embed(src)
        x = self.pe(x)
        # x = self.norm(x)
        x = self.layer(x, mask)
        return self.norm(x)


class Decoder(nn.Module):
    def __init__(self, attn, embed, pe, norm, ff, dropout_layer,
                 vocab_size, d_vocab, d_hidden, heads,
                 max_seq_len):
        super().__init__()
        self.embed = embed
        self.pe = pe
        self.layer = DecoderLayer(attn, norm, ff, dropout_layer,
                                  d_hidden, heads)
        self.dropout = dropout_layer
        self.norm = norm

    def forward(self, trg, e_outputs, src_mask, trg_mask):
        x = self.embed(trg)
        x = self.pe(x)
        # x = self.norm(x)
        x = self.layer(x, e_outputs, src_mask, trg_mask)
        return self.norm(x)


class Transformer(nn.Module):
    def __init__(self, vocab_size, d_vocab, d_hidden,
                 heads, dropout, max_seq_len):
        super().__init__()
        self.pe = PositionalEncoder(d_hidden, dropout=dropout,
                                    max_seq_len=max_seq_len)
        self.embed = Embedder(vocab_size, d_vocab, d_hidden, max_seq_len)
        self.norm = Norm(d_hidden)
        self.dropout_layer = nn.Dropout(dropout)
        self.attn = MultiHeadAttention(heads, d_hidden, self.dropout_layer)
        self.ff = FeedForward(d_hidden, self.dropout_layer)
        self.encoder = Encoder(self.attn, self.embed, self.pe,
                               self.norm, self.ff, self.dropout_layer,
                               vocab_size, d_vocab, d_hidden, heads,
                               max_seq_len)
        self.decoder = Decoder(self.attn, self.embed, self.pe,
                               self.norm, self.ff, self.dropout_layer,
                               vocab_size, d_vocab, d_hidden, heads,
                               max_seq_len)
        # self.hidden_to_vocab = nn.Linear(d_hidden, d_vocab, bias=False)
        # self.vocab_to_token = nn.Linear(d_vocab, vocab_size, bias=False)
        # print('self.hidden_to_vocab.weight.data.size()', self.hidden_to_vocab.weight.data.size())
        # print('self.vocab_to_token.weight.data.size()', self.vocab_to_token.weight.data.size())
        print('d_vocab', d_vocab)
        print('vocab_size', vocab_size)
        # # sharing weights between input and output embedding
        # self.hidden_to_vocab.weight.data = self.embed.hidden_embed.weight.data.t()
        # self.vocab_to_token.weight.data = self.embed.vocab_embed.weight.data.t()
        # print('self.hidden_to_vocab.weight.data.size()', self.hidden_to_vocab.weight.data.size())
        print('self.embed.hidden_embed.weight.data.size()', self.embed.hidden_embed.weight.data.size())
        # print('self.vocab_to_token.weight.data.size()', self.vocab_to_token.weight.data.size())
        print('self.embed.vocab_embed.weight.data.size()', self.embed.vocab_embed.weight.data.size())

    def forward(self, src, trg, src_mask, trg_mask):
        e_outputs = self.encoder(src, src_mask)
        d_output = self.decoder(trg, e_outputs, src_mask, trg_mask)
        # output_vocab = self.hidden_to_vocab(d_output)
        # output_token = self.vocab_to_token(output_vocab)
        output_vocab = F.linear(d_output, self.embed.hidden_embed.weight.data.t())
        output_token = F.linear(output_vocab, self.embed.vocab_embed.weight.data)
        return output_token


def get_model(args):
    assert args.d_hidden % args.heads == 0
    assert args.dropout < 1

    model = Transformer(args.src_vocab_size, args.d_vocab, args.d_hidden,
                        args.heads, args.dropout, args.max_tokens)

    model_file = args.ckpt_file or 'model_weights.ckpt'
    model_path = os.path.join(args.load_weights, model_file)
    if args.load_weights is not None \
            and os.path.exists(model_path):
        print("loading pretrained weights from", model_path)
        model.load_state_dict(torch.load(model_path))
    else:
        for p in model.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)
    if args.device == 0:
        model = model.cuda()
    return model


def nopeak_mask(size, args):
    np_mask = np.triu(np.ones((1, size, size)),
                      k=1).astype('uint8')
    np_mask = Variable(torch.from_numpy(np_mask) == 0)
    # np_mask.size = 1 * seq_len+1 * seq_len+1
    if args.device == 0:
        np_mask = np_mask.cuda()
    return np_mask


def create_masks(src, trg, args):
    src_mask = (src != args.pad_id).unsqueeze(-2)
    if trg is not None:
        trg_mask = (trg != args.pad_id).unsqueeze(-2)
        size = trg.size(1)  # get seq_len for matrix
        np_mask = nopeak_mask(size, args)
        if trg.is_cuda:
            np_mask.cuda()
        trg_mask = trg_mask & np_mask
    else:
        trg_mask = None
    # src_mask & trg_mask dimensions are batch_size * 1 * seq_len
    return src_mask, trg_mask
