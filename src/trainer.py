# coding: utf-8
import torch
from time import time
import os
import numpy as np
from src.data import mask_source
from src.model import create_masks
from src.optim import CosineWithRestarts, ls_loss
import json
import math


def train_model(args):
    print("training model...")
    model = args.model
    model.train()
    start = time()
    args.optimizer = torch.optim.Adam(model.parameters(), lr=args.lr,
                                      betas=(0.9, 0.98), eps=1e-9,
                                      amsgrad=False)

    if args.SGDR is True:
        args.sched = CosineWithRestarts(args.optimizer, T_max=args.train_len)
        import matplotlib.pyplot as plt
        plt.plot(np.arange(1, 20000), [args.sched.get_lr()
                                       for i in range(1, 20000)])
        plt.show()

    if args.checkpoint > 0:
        print("model weights will be saved every {:d} epochs "
              "to directory {}/".format(args.checkpoint,
                                        args.load_weights or 'weights'))

    if args.load_weights is not None:
        if not os.path.isdir(args.load_weights):
            os.mkdir(args.load_weights)

    history = dict()
    history['loss'] = list()
    history['accuracy'] = list()
    history['runtime'] = list()
    history['settings'] = {
        'd_vocab': args.d_vocab,
        'd_hidden': args.d_hidden,
        'heads': args.heads,
        'dropout': args.dropout,
        'label_smoothing': args.label_smoothing,
        'cut_and_swap_prob': args.cut_and_swap_prob,
        'mask_prob': args.mask_prob,
        'insert_suppr_prob': args.insert_suppr_prob,
        'lr': args.lr,
        'batch_size': args.batch_size,
    }
    for epoch in range(args.epochs):
        total_loss = 0
        total_accuracy = 0
        for i, (src, trg, src_counts) in enumerate(args.train_iterator):
            src = mask_source(src, src_counts, args)
            trg_input = trg[:, :-1]
            gold = trg[:, 1:]
            src_mask, trg_mask = create_masks(src, trg_input, args)
            preds = model(src, trg_input, src_mask, trg_mask)
            preds_labels = preds.argmax(dim=2)
            nb_correct = (preds_labels == gold).float().sum()
            accuracy = nb_correct / (preds.size(0)*preds.size(1))
            total_accuracy += float(accuracy)
            ys = gold.contiguous().view(-1)
            args.optimizer.zero_grad()
            loss = ls_loss(preds.contiguous().view(-1, preds.size(-1)),
                           ys, args.label_smoothing, args.pad_id)
            loss.backward()
            args.optimizer.step()
            if args.SGDR is True:
                args.sched.step()

            cur_loss = loss.item() / args.batch_size
            total_loss += float(cur_loss)

            if (i + 1) % args.printevery == 0:
                p = int(100 * (i + 1) / args.batch_count)
                avg_loss = round(total_loss / args.printevery, 4)
                avg_accuracy = round(total_accuracy / args.printevery, 4)
                print("%dm: epoch %d [%s%s]  %d%%  avg_loss = %.3f "
                      "avg_accuracy = %.3f " %
                      ((time() - start)//60, epoch + 1, "".join('#'*(p//5)),
                       "".join(' '*(20-(p//5))), p, avg_loss, avg_accuracy),
                      end='\r')
                total_loss = 0
                total_accuracy = 0

        if args.checkpoint > 0 and (epoch + 1) % args.checkpoint == 0:
            torch.save(model.state_dict(),
                       '{}/model_weights_e{}_loss{}_acc{}.ckpt'.format(
                            args.load_weights, epoch + 1,
                            avg_loss, avg_accuracy))
        print(" "*110, end='\r')
        print("%dm: epoch %d complete, avg_loss = %.03f "
              "avg_accuracy = %.3f " %
              ((time() - start)//60, epoch + 1, avg_loss, avg_accuracy))
        # print('pe scale', model.pe.scale)
        history['loss'].append(avg_loss)
        history['accuracy'].append(avg_accuracy)
        history['runtime'].append((time() - start)//60)
        with open(os.path.join(args.load_weights, 'history.json'), 'w') as f:
            json.dump(history, f)
    torch.save(model.state_dict(),
               os.path.join(args.load_weights, args.ckpt_file))
