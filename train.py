# coding: utf-8
import argparse
from src.data import process_data, mask_source
from src.model import get_model
from src.trainer import train_model
import torch


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-src', default='data/all_lines_cleaned.txt')
    # parser.add_argument('-src', default='data/english.txt')
    parser.add_argument('-no_cuda', action='store_true')
    parser.add_argument('-SGDR', action='store_true', default=False)
    parser.add_argument('-epochs', type=int, default=150)
    parser.add_argument('-load_weights', default='weights')
    parser.add_argument('-ckpt_file', default='model_weights.ckpt')
    parser.add_argument('-d_vocab', type=int, default=256)
    parser.add_argument('-d_hidden', type=int, default=1024)
    parser.add_argument('-heads', type=int, default=16)
    parser.add_argument('-dropout', type=float, default=0.0)
    parser.add_argument('-label_smoothing', type=float, default=0.0)
    parser.add_argument('-mask_prob', type=float, default=0.15)
    parser.add_argument('-cut_and_swap_prob', type=float, default=0.34)
    parser.add_argument('-insert_suppr_prob', type=float, default=0.0)
    parser.add_argument('-batch_size', type=int, default=32+16)
    parser.add_argument('-printevery', type=int, default=5)
    parser.add_argument('-lr', type=float, default=0.0001)
    # parser.add_argument('-max_tokens', type=int, default=16)
    parser.add_argument('-checkpoint', type=int, default=4)

    args = parser.parse_args()

    args.device = 0 if args.no_cuda is False else -1
    if args.device == 0:
        assert torch.cuda.is_available()
    print('Device :', 'cuda ' if args.device == 0 else 'cpu')
    print('Vocab embedding :', str(args.d_vocab))
    print('Model embedding :', str(args.d_hidden))
    print('Heads :', str(args.heads))
    print('Batch size :', str(args.batch_size))
    print('Label smoothing :', str(args.label_smoothing))
    print('Dropout :', str(args.dropout))
    print('Mask prob :', str(args.mask_prob))
    print('Cut & swap prob :', str(args.cut_and_swap_prob))
    print('Insert/suppr prob :', str(args.insert_suppr_prob))

    args.train_iterator = process_data(args)
    args.model = get_model(args)

    total_trainable_params = sum(p.numel() for p in args.model.parameters()
                                 if p.requires_grad)
    total_params = sum(p.numel() for p in args.model.parameters())
    print('Number of trainable parameters :', total_trainable_params,
          '/', total_params)

    batch_count = 0
    count = 0
    src_pad_count = 0
    src_tok_count = 0
    for i, (src, trg, src_counts) in enumerate(args.train_iterator):
        batch_count += 1
        src = mask_source(src, src_counts, args)
        # print('batch n°'+str(batch_count), 'of size', src.size(0))
        for s, t in zip(src, trg):
            # print([args.src_id2t[str(toks)] for toks in s.cpu().numpy().tolist()])
            # print([args.src_id2t[str(toks)] for toks in t.cpu().numpy().tolist()])
            for toks in s.cpu().numpy().tolist():
                src_tok_count += 1
                if toks == args.pad_id:
                    src_pad_count += 1
            count += 1
        # quit()
    print('Number of examples iterated :', count)
    print('Batch count :', batch_count)
    print('Src waste (pad tokens) :', str(src_pad_count), '/',
          str(src_tok_count), '=',
          str(src_pad_count / src_tok_count * 100), '%')

    train_model(args)

if __name__ == "__main__":
    main()
